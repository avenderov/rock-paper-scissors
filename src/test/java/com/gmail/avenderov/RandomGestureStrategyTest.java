package com.gmail.avenderov;

import org.junit.Test;

import java.util.Random;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author avenderov
 */
public class RandomGestureStrategyTest {

    @Test
    public void testNextGesture() {
        final Random mockRandom = mock(Random.class);
        when(mockRandom.nextInt(anyInt())).thenReturn(2);

        final Strategy strategy = new RandomGestureStrategy(mockRandom);
        final Gesture nextGesture = strategy.nextGesture();

        assertThat("Wrong next gesture returned", nextGesture,
                is(equalTo(Gesture.SCISSORS)));
        verify(mockRandom, times(1)).nextInt(eq(Gesture.values().length));
    }

}
