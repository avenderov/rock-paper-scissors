package com.gmail.avenderov;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author avenderov
 */
public class GestureTest {

    @Test(expected = IllegalArgumentException.class)
    public void testFromSymbolWhenSymbolIsOfLength2() {
        Gesture.fromSymbol("RR");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromSymbolWhenNoSuchGesture() {
        Gesture.fromSymbol("A");
    }

    @Test
    public void testSymbolIsKeyInsensitive() {
        assertThat("Wrong gesture returned for the symbol \"s\"",
                Gesture.fromSymbol("s"), is(equalTo(Gesture.SCISSORS)));
    }
}
