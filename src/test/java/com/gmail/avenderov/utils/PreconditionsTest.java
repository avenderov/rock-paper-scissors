package com.gmail.avenderov.utils;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author avenderov
 */
public class PreconditionsTest {

    @Test
    public void testCheckNotNullThrowsAnExceptionWithSpecifiedMessage() {
        try {
            Preconditions.checkNotNull(null, "Test error message");
            fail("NPE should be thrown if object reference is null");
        } catch (NullPointerException e) {
            assertThat("NPE should have custom error message", e.getMessage(),
                    is(equalTo("Test error message")));
        }
    }

    @Test
    public void testCheckNotNullThrowsAnExceptionWithDefaultMessage() {
        try {
            Preconditions.checkNotNull(null, null);
            fail("NPE should be thrown if object reference is null");
        } catch (NullPointerException e) {
            assertThat(
                    "NPE should have default error message if user didn't specify it",
                    e.getMessage(), is(equalTo(
                    Preconditions.DEFAULT_NULL_REFERENCE_ERROR_MESSAGE)));
        }
    }

    @Test
    public void testCheckArgumentThrowsAnExceptionWithSpecifiedMessage() {
        try {
            Preconditions.checkArgument(false, "Test error message");
            fail("IAE should be thrown if expression is false");
        } catch (IllegalArgumentException e) {
            assertThat("IAE should have custom error message", e.getMessage(),
                    is(equalTo("Test error message")));
        }
    }

    @Test
    public void testCheckArgumentThrowsAnExceptionWithDefaultMessage() {
        try {
            Preconditions.checkArgument(false, null);
            fail("IAE should be thrown if expression is false");
        } catch (IllegalArgumentException e) {
            assertThat(
                    "IAE should have default error message if user didn't specify it",
                    e.getMessage(), is(equalTo(
                    Preconditions.DEFAULT_CHECK_ARGUMENT_ERROR_MESSAGE)));
        }
    }

}
