package com.gmail.avenderov.utils;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author avenderov
 */
public class CollectionUtilsTest {

    @Test
    public void testToStringEmptyCollection() {
        assertThat(
                "String should contain only square brackets if collection is empty",
                CollectionUtils.toString(Collections.<String>emptyList(), "/"),
                is(equalTo("[]")));
    }

    @Test
    public void testToString() {
        assertThat("Wrong collection string representation",
                CollectionUtils.toString(Arrays.asList("a", "b", "c"), "-"),
                is(equalTo("[a-b-c]")));
    }

}
