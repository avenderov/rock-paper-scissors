package com.gmail.avenderov.utils;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author avenderov
 */
public class StringUtilsTest {

    @Test
    public void testNullStringIsEmpty() {
        assertThat("isEmpty must return true for null string",
                StringUtils.isEmpty(null), is(true));
        assertThat("isNotEmpty must return false for null string",
                StringUtils.isNotEmpty(null), is(false));
    }

    @Test
    public void testIsEmpty() {
        assertThat("isEmpty returned wrong result for empty string",
                StringUtils.isEmpty(""), is(true));
        assertThat("isEmpty returned wrong result for string \"test\"",
                StringUtils.isEmpty("test"), is(false));
    }

    @Test
    public void testIsNotEmpty() {
        assertThat("isNotEmpty returned wrong result for string \"test\"",
                StringUtils.isNotEmpty("test"), is(true));
    }

}
