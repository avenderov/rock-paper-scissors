package com.gmail.avenderov.utils;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author avenderov
 */
public class ConsoleUtilsTest {

    private Console mockConsole;

    private MessageResource mockMessageResource;

    @Before
    public void setUp() {
        mockConsole = mock(Console.class);
        mockMessageResource = mock(MessageResource.class);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNumberOfOptionsIsLessThanTwo() {
        ConsoleUtils
                .askUserAndGetChoice(mockConsole, mockMessageResource, "test",
                        new String[]{"Y"});
    }

    @Test
    public void testReturnedValueIsTrimmed() {
        when(mockMessageResource.getString(eq("test"))).thenReturn("test");
        when(mockConsole.readLine(anyString(), anyVararg())).thenReturn(" Y ");

        final String result = ConsoleUtils
                .askUserAndGetChoice(mockConsole, mockMessageResource, "test",
                        new String[]{"Y", "N"});
        assertThat("Selected option must be trimmed", result, is(equalTo("Y")));
    }

    @Test
    public void testLowerCaseOptionsCanBeSelected() {
        final ArgumentCaptor<String> allOptionsCaptor =
                ArgumentCaptor.forClass(String.class);

        when(mockMessageResource.getString(eq("test"))).thenReturn("test");
        when(mockConsole.readLine(anyString(), anyVararg())).thenReturn("Y");

        ConsoleUtils.askUserAndGetChoice(mockConsole, mockMessageResource,
                "test", new String[]{"Y", "N"});
        verify(mockConsole).readLine(anyString(), allOptionsCaptor.capture());
        assertThat("Wrong all options value captured",
                allOptionsCaptor.getValue(), is(equalTo("[Y/y/N/n]")));
    }

    @Test
    public void testPromptAgainIfUserSelectedIncorrectOption() {
        when(mockMessageResource.getString(eq("test"))).thenReturn("test");
        when(mockConsole.readLine(anyString(), anyVararg()))
                .thenReturn("R", "Y");

        ConsoleUtils.askUserAndGetChoice(mockConsole, mockMessageResource,
                "test", new String[]{"Y", "N"});
        verify(mockConsole, times(2)).readLine(anyString(), anyVararg());
    }

}
