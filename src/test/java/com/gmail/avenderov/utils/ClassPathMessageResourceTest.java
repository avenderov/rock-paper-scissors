package com.gmail.avenderov.utils;

import com.gmail.avenderov.exceptions.ResourceBundleNotFoundException;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author avenderov
 */
public class ClassPathMessageResourceTest {

    @Test(expected = IllegalArgumentException.class)
    public void testCreateUsingNullBundleName() throws Exception {
        ClassPathMessageResource.of(null);
    }

    @Test(expected = ResourceBundleNotFoundException.class)
    public void testCreateUsingNonExistingBundleName() throws Exception {
        ClassPathMessageResource.of("nonExistingBundle");
    }

    @Test
    public void testGetStringPropertyValue() throws Exception {
        final MessageResource messageResource =
                ClassPathMessageResource.of("test");
        assertThat("Unexpected \"name\" property value in \"test\" bundle",
                messageResource.getString("name"), is(equalTo("Alexey")));
    }

    @Test
    public void testGetNonExitingPropertyValue() throws Exception {
        final MessageResource messageResource =
                ClassPathMessageResource.of("test");
        assertThat(
                "Key value should be returned if there is no value for that key in a bundle",
                messageResource.getString("surname"), is(equalTo("surname")));
    }

}
