package com.gmail.avenderov;

import org.junit.Test;

import java.util.Random;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author avenderov
 */
public class RandomGameTypeSelectorTest {

    @Test
    public void testNextGameType() {
        final Random mockRandom = mock(Random.class);
        when(mockRandom.nextInt(anyInt())).thenReturn(1);

        final GameTypeSelector gameTypeSelector =
                new RandomGameTypeSelector(mockRandom);
        final GameType nextGameType = gameTypeSelector.nextGameType();

        assertThat("Wrong next game type returned", nextGameType,
                is(equalTo(GameType.COMPUTER_VS_USER)));
        verify(mockRandom, times(1)).nextInt(eq(GameType.values().length));
    }

}
