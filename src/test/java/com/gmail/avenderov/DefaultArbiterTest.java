package com.gmail.avenderov;

import org.junit.BeforeClass;
import org.junit.Test;

import static java.lang.String.format;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author avenderov
 */
public class DefaultArbiterTest {

    private static Arbiter arbiter;

    @BeforeClass
    public static void createNewArbiter() {
        arbiter = new DefaultArbiter();
    }

    @Test
    public void testEqualGestures() {
        checkGameResultForGestures(Gesture.PAPER, Gesture.PAPER,
                Arbiter.GameResult.DRAW);
        checkGameResultForGestures(Gesture.ROCK, Gesture.ROCK,
                Arbiter.GameResult.DRAW);
        checkGameResultForGestures(Gesture.SCISSORS, Gesture.SCISSORS,
                Arbiter.GameResult.DRAW);
    }

    private static void checkGameResultForGestures(final Gesture firstGesture,
                                                   final Gesture secondGesture,
                                                   final Arbiter.GameResult expectedGameResult) {
        final Arbiter.GameResult gameResult =
                arbiter.judge(firstGesture, secondGesture);
        assertThat(format("Wrong game result for gestures %1$s and %2$s",
                firstGesture, secondGesture), gameResult,
                is(equalTo(expectedGameResult)));
    }

    @Test
    public void testFirstGestureBeatsSecond() {
        checkGameResultForGestures(Gesture.PAPER, Gesture.ROCK,
                Arbiter.GameResult.WON);
        checkGameResultForGestures(Gesture.ROCK, Gesture.SCISSORS,
                Arbiter.GameResult.WON);
        checkGameResultForGestures(Gesture.SCISSORS, Gesture.PAPER,
                Arbiter.GameResult.WON);
    }

    @Test
    public void testSecondGestureBeatsFirst() {
        checkGameResultForGestures(Gesture.PAPER, Gesture.SCISSORS,
                Arbiter.GameResult.LOST);
        checkGameResultForGestures(Gesture.ROCK, Gesture.PAPER,
                Arbiter.GameResult.LOST);
        checkGameResultForGestures(Gesture.SCISSORS, Gesture.ROCK,
                Arbiter.GameResult.LOST);
    }

}
