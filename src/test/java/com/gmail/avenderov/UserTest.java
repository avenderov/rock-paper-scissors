package com.gmail.avenderov;

import com.gmail.avenderov.utils.Console;
import com.gmail.avenderov.utils.MessageResource;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author avenderov
 */
public class UserTest {

    @Test
    public void testNextGestureIsReadFromConsole() {
        final Console mockConsole = mock(Console.class);
        final MessageResource mockMessageResource = mock(MessageResource.class);

        when(mockMessageResource.getString(eq("throw.gesture")))
                .thenReturn("test");
        when(mockConsole.readLine(anyString(), anyVararg())).thenReturn("R");

        final Player user = new User(mockConsole, mockMessageResource);
        final Gesture gesture = user.throwGesture();

        verify(mockConsole, times(1)).readLine(eq("test"), eq("[R/r/P/p/S/s]"));
        assertThat("Wrong gesture was thrown", gesture,
                is(equalTo(Gesture.ROCK)));
    }

}
