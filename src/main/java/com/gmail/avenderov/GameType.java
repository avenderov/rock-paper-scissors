package com.gmail.avenderov;

/**
 * Available game types.
 *
 * @author avenderov
 */
public enum GameType {

    COMPUTER_VS_COMPUTER("computer.vs.computer.game"),
    COMPUTER_VS_USER("computer.vs.user.game");

    private final String descriptionKey;

    private GameType(final String descriptionKey) {
        this.descriptionKey = descriptionKey;
    }

    /**
     * Returns message bundle key that should be used to retrieve description.
     *
     * @return message bundle key
     */
    public String getDescriptionKey() {
        return descriptionKey;
    }

}
