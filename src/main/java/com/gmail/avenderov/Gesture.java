package com.gmail.avenderov;

import static com.gmail.avenderov.utils.Preconditions.checkArgument;
import static java.lang.String.format;

/**
 * Gestures that players can "throw". Sequence of elements in this enumeration
 * is vital for algorithm that is used to judge game result. Every subsequent
 * gesture should defeat preceding. Amount of elements in enum should be odd and
 * greater or equal to 3.
 *
 * @author avenderov
 */
public enum Gesture {

    ROCK("R"),
    PAPER("P"),
    SCISSORS("S");

    private final String symbol;

    private Gesture(final String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    /**
     * Returns {@link Gesture} that is mapped to specified symbol.
     *
     * @param symbol to get {@link Gesture} for
     *
     * @return an instance of {@link Gesture}
     * @throws IllegalArgumentException if symbol is {@code null}, or it's
     *                                  length is not equal to 1, or there is no
     *                                  corresponding {@link Gesture}
     */
    public static Gesture fromSymbol(final String symbol) {
        checkArgument(symbol != null && symbol.length() == 1,
                "symbol length must be 1");

        if ("R".equalsIgnoreCase(symbol)) {
            return ROCK;
        } else if ("P".equalsIgnoreCase(symbol)) {
            return PAPER;
        } else if ("S".equalsIgnoreCase(symbol)) {
            return SCISSORS;
        }

        throw new IllegalArgumentException(
                format("There is no gesture for symbol: %1$s", symbol));
    }

}
