package com.gmail.avenderov;

/**
 * This {@link Arbiter} implementation is based on an order of elements in
 * {@link Gesture} enum.
 *
 * @author avenderov
 */
public class DefaultArbiter implements Arbiter {

    private final int numberOfGestures;

    public DefaultArbiter() {
        numberOfGestures = Gesture.values().length;
    }

    @Override
    public GameResult judge(final Gesture firstGesture,
                            final Gesture secondGesture) {
        final int result = (numberOfGestures + firstGesture.ordinal() -
                secondGesture.ordinal()) % numberOfGestures;
        if (result == 0) {
            return GameResult.DRAW;
        }
        return isEven(result) ? GameResult.LOST : GameResult.WON;
    }

    private static boolean isEven(final int value) {
        return (value & 1) == 0;
    }

}
