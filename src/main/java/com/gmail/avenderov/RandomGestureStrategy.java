package com.gmail.avenderov;

import java.util.Random;

/**
 * {@link Strategy} implementation that returns random {@link Gesture} on each
 * invocation.
 *
 * @author avenderov
 */
public class RandomGestureStrategy implements Strategy {

    private final Random random;

    public RandomGestureStrategy() {
        this(new Random());
    }

    // This constructor should only be used in tests
    RandomGestureStrategy(final Random random) {
        this.random = random;
    }

    @Override
    public Gesture nextGesture() {
        final int nextGestureIndex = random.nextInt(Gesture.values().length);
        return Gesture.values()[nextGestureIndex];
    }

}
