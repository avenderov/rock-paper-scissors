package com.gmail.avenderov;

import java.util.Random;

/**
 * {@link GameTypeSelector} that returns random {@link GameType} on each
 * invocation.
 *
 * @author avenderov
 */
public class RandomGameTypeSelector implements GameTypeSelector {

    private final Random random;

    public RandomGameTypeSelector() {
        this(new Random());
    }

    // This constructor should only be used in tests
    RandomGameTypeSelector(final Random random) {
        this.random = random;
    }

    @Override
    public GameType nextGameType() {
        final int nextGameTypeIndex = random.nextInt(GameType.values().length);
        return GameType.values()[nextGameTypeIndex];
    }

}
