package com.gmail.avenderov.utils;

import static com.gmail.avenderov.utils.Preconditions.checkNotNull;

/**
 * Immutable pair of objects that have to be treated like one object.
 *
 * @author avenderov
 */
public class Pair<L, R> {

    private final L left;

    private final R right;

    private Pair(final L left, final R right) {
        this.left = left;
        this.right = right;
    }

    /**
     * Returns left object from the pair.
     *
     * @return left object instance
     */
    public L getLeft() {
        return left;
    }

    /**
     * Returns right object from the pair.
     *
     * @return right object instance
     */
    public R getRight() {
        return right;
    }

    /**
     * Returns left object from the pair.
     *
     * @return left object instance
     */
    public L getFirst() {
        return getLeft();
    }

    /**
     * Returns right object from the pair.
     *
     * @return right object instance
     */
    public R getSecond() {
        return getRight();
    }

    /**
     * Factory method that is used to create {@code Pair} instances.
     *
     * @param left left object instance
     * @param right right object instance
     *
     * @return a new instance of {@code Pair} class
     * @throws NullPointerException if left or right object is {@code null}
     */
    public static <L, R> Pair<L, R> of(final L left, final R right) {
        checkNotNull(left, "left object must not be null");
        checkNotNull(left, "right object must not be null");

        return new Pair<L, R>(left, right);
    }

}
