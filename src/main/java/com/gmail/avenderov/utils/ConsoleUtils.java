package com.gmail.avenderov.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import static com.gmail.avenderov.utils.Preconditions.checkArgument;
import static com.gmail.avenderov.utils.Preconditions.checkNotNull;
import static com.gmail.avenderov.utils.StringUtils.isEmpty;

/**
 * Various methods that simplifies console interaction.
 *
 * @author avenderov
 */
public class ConsoleUtils {

    private ConsoleUtils() {
    }

    /**
     * Shows prompt to the user and waits for user to choose. User can select
     * only from options specified with {@code options} parameter. If user
     * enters any other value, prompt will be shown again.
     *
     * @param console          object that represents console device
     * @param messageResource  message resources
     * @param messageFormatKey a key for the message to show to the user
     * @param options          array of options that user can select. All values
     *                         should be in upper case and in {@link
     *                         Locale#ENGLISH} locale
     *
     * @return one of the options that user has selected. Returned value is case
     *         insensitive (e.g. may be in upper or lower case). Returned value
     *         will be trimmed
     * @throws NullPointerException     if {@code console}, {@code messageResource}
     *                                  or {@code messageFormatKey} is {@code
     *                                  null}
     * @throws IllegalArgumentException if options is {@code null} or contains
     *                                  less than 2 elements
     */
    public static String askUserAndGetChoice(final Console console,
                                             final MessageResource messageResource,
                                             final String messageFormatKey,
                                             final String[] options) {
        checkNotNull(console, "console must not be null");
        checkNotNull(messageResource, "messageResource must not be null");
        checkNotNull(messageFormatKey, "messageFormatKey must not be null");
        checkArgument(options != null && options.length >= 2,
                "number of options must be greater or equal to 2");

        final Collection<String> allOptions = getAllOptions(options);

        String input;
        do {
            input = console.readLine(
                    messageResource.getString(messageFormatKey),
                    CollectionUtils.toString(allOptions, "/"));
        } while (isEmpty(input) || !allOptions.contains(input.trim()));

        return input.trim();
    }

    private static Collection<String> getAllOptions(final String[] options) {
        // We need to double size, because options can be in upper or lower case
        final List<String> result = new ArrayList<String>(options.length * 2);
        for (final String option : options) {
            result.add(option);
            // This method assumes that options are always in upper case and in
            // english locale
            result.add(option.toLowerCase(Locale.ENGLISH));
        }
        return result;
    }

}
