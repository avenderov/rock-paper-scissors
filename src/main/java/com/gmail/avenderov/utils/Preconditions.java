package com.gmail.avenderov.utils;

/**
 * Contains different methods that are used to validate method arguments. This
 * class can't be instantiated. All methods in this class should be {@code
 * static}.
 *
 * @author avenderov
 */
public class Preconditions {

    // Constants has "default" visibility to be visible in tests
    static final String DEFAULT_NULL_REFERENCE_ERROR_MESSAGE =
            "Object reference is null";

    static final String DEFAULT_CHECK_ARGUMENT_ERROR_MESSAGE =
            "Expression must be true";

    /**
     * This class can't be instantiated and sub classed.
     */
    private Preconditions() {
    }

    /**
     * Checks if passed object reference is not {@code null}.
     *
     * @param reference    a reference to check
     * @param errorMessage exception error message. May be {@code null}
     *
     * @throws NullPointerException if object reference is {@code null}
     */
    public static <T> void checkNotNull(final T reference,
                                        final String errorMessage) {
        if (reference == null) {
            throw new NullPointerException(errorMessage != null ? errorMessage :
                    DEFAULT_NULL_REFERENCE_ERROR_MESSAGE);
        }
    }

    /**
     * Checks if passed expression is {@code true}.
     *
     * @param expression   expression to check
     * @param errorMessage exception error message. May be {@code nullss}
     *
     * @throws IllegalArgumentException if expression is {@code false}
     */
    public static void checkArgument(final boolean expression,
                                     final String errorMessage) {
        if (!expression) {
            throw new IllegalArgumentException(
                    errorMessage != null ? errorMessage :
                            DEFAULT_CHECK_ARGUMENT_ERROR_MESSAGE);
        }
    }

}
