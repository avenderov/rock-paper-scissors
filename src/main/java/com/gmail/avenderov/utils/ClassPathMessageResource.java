package com.gmail.avenderov.utils;

import com.gmail.avenderov.exceptions.ResourceBundleNotFoundException;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.gmail.avenderov.utils.Preconditions.checkArgument;
import static com.gmail.avenderov.utils.StringUtils.isNotEmpty;
import static java.lang.String.format;

/**
 * This class is used to retrieve messages from message bundles located in
 * classpath.
 *
 * @author avenderov
 */
public class ClassPathMessageResource implements MessageResource {

    private static final Logger LOGGER =
            Logger.getLogger(ClassPathMessageResource.class.getName());

    private final ResourceBundle resourceBundle;

    /**
     * This class should be instantiated using {@linkplain #of(String)} factory
     * method.
     */
    private ClassPathMessageResource(final ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    @Override
    public String getString(final String key) {
        try {
            return resourceBundle.getString(key);
        } catch (MissingResourceException e) {
            LOGGER.log(Level.WARNING,
                    format("There is no \"%1$s\" key in a bundle", key), e);
            return key;
        }
    }

    /**
     * Creates instances of {@link ClassPathMessageResource}.
     *
     * @param bundle bundle name to search for in classpath
     *
     * @return new {@link ClassPathMessageResource} instance
     * @throws ResourceBundleNotFoundException
     *                                  if bundle with specified name was not
     *                                  found in classpath
     * @throws IllegalArgumentException if specified bundle is empty
     */
    public static MessageResource of(final String bundle) throws
            ResourceBundleNotFoundException {
        checkArgument(isNotEmpty(bundle), "bundle must not be empty");
        try {
            final ResourceBundle resourceBundle =
                    ResourceBundle.getBundle(bundle);
            return new ClassPathMessageResource(resourceBundle);
        } catch (MissingResourceException e) {
            throw new ResourceBundleNotFoundException(
                    format("Resource bundle with a name %1$s was not found in classpath",
                            bundle), e);
        }
    }

}
