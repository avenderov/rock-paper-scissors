package com.gmail.avenderov.utils;

/**
 * This interface represents a console inside the application. As soon as {@link
 * java.io.Console} class is declared final it is hard to test classes that uses
 * it. This interface will be used in classes that have to interact with console
 * to simplify their testing.
 *
 * @author avenderov
 */
public interface Console {

    /**
     * @see java.io.Console#format(String, Object...)
     */
    void format(String fmt, Object... args);

    /**
     * @see java.io.Console#printf(String, Object...)
     */
    void printf(String format, Object... args);

    /**
     * @see java.io.Console#readLine(String, Object...)
     */
    String readLine(String fmt, Object... args);

    /**
     * @see java.io.Console#readLine()
     */
    String readLine();

}
