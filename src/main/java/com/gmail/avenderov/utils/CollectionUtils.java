package com.gmail.avenderov.utils;

import java.util.Collection;

import static com.gmail.avenderov.utils.Preconditions.checkNotNull;

/**
 * Various utilities for collections.
 *
 * @author avenderov
 */
public class CollectionUtils {

    private CollectionUtils() {
    }

    /**
     * Joins collection elements into a string with specified delimiter.
     * Resulting string is wrapped into square brackets.
     *
     * @param collection a collection to join it's elements
     * @param delimiter  delimiter to be used between elements
     *
     * @return collection elements joined into string
     * @throws NullPointerException if collection or delimiter is {@code null}
     */
    public static <T> String toString(final Collection<T> collection,
                                      final String delimiter) {
        checkNotNull(collection, "collection must not be null");
        checkNotNull(delimiter, "delimiter must not be null");

        final StringBuilder result = new StringBuilder("[");
        int index = 0;
        for (final T element : collection) {
            result.append(element);
            if (index < collection.size() - 1) {
                result.append(delimiter);
            }
            index++;
        }
        result.append("]");
        return result.toString();
    }

}
