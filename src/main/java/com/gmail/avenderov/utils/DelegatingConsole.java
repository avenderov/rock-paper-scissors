package com.gmail.avenderov.utils;

import static com.gmail.avenderov.utils.Preconditions.checkNotNull;

/**
 * This {@link Console} implementation delegates all calls to underlying {@link
 * java.io.Console} object.
 *
 * @author avenderov
 */
public class DelegatingConsole implements Console {

    private final java.io.Console console;

    public DelegatingConsole(final java.io.Console console) {
        checkNotNull(console, "console must not be null");
        this.console = console;
    }

    @Override
    public void format(final String fmt, final Object... args) {
        console.format(fmt, args);
    }

    @Override
    public void printf(final String format, final Object... args) {
        console.printf(format, args);
    }

    @Override
    public String readLine(final String fmt, final Object... args) {
        return console.readLine(fmt, args);
    }

    @Override
    public String readLine() {
        return console.readLine();
    }

}
