package com.gmail.avenderov.utils;

/**
 * Contains different methods that are used to manipulate strings. This class
 * can't be instantiated. All methods in this class should be {@code static}.
 *
 * @author avenderov
 */
public class StringUtils {

    /**
     * This class can't be instantiated and sub classed.
     */
    private StringUtils() {
    }

    /**
     * Checks if specified string value is not empty. This method is {@code
     * null} safe.
     *
     * @param value string value to check
     *
     * @return {@code true} if specified string is not empty
     * @see StringUtils#isEmpty(String)
     */
    public static boolean isNotEmpty(final String value) {
        return !isEmpty(value);
    }

    /**
     * Checks if specified string value is empty. This method is {@code null}
     * safe.
     *
     * @param value string value to check
     *
     * @return {@code true} if specified string is {@code null} or empty
     */
    public static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

}
