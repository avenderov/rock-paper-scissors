package com.gmail.avenderov.utils;

/**
 * Message resources that can be internationalized.
 *
 * @author avenderov
 */
public interface MessageResource {

    /**
     * Retrieves message by specified {@code key} from message bundle.
     *
     * @param key message property key
     *
     * @return message for specified key or key itself if there is no such key
     *         in a bundle
     */
    String getString(String key);

}
