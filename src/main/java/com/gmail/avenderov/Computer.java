package com.gmail.avenderov;

import static com.gmail.avenderov.utils.Preconditions.checkNotNull;

/**
 * {@link Player} implementation that represents computer player.
 *
 * @author avenderov
 */
public class Computer implements Player {

    private final Strategy strategy;

    public Computer(final Strategy strategy) {
        checkNotNull(strategy, "strategy must not be null");
        this.strategy = strategy;
    }

    @Override
    public Gesture throwGesture() {
        return strategy.nextGesture();
    }

}
