package com.gmail.avenderov;

import com.gmail.avenderov.utils.Console;
import com.gmail.avenderov.utils.ConsoleUtils;
import com.gmail.avenderov.utils.MessageResource;

import java.util.ArrayList;
import java.util.List;

import static com.gmail.avenderov.utils.Preconditions.checkNotNull;

/**
 * {@link Player} implementation that represents human player.
 *
 * @author avenderov
 */
public class User implements Player {

    private final Console console;

    private final MessageResource messageResource;

    public User(final Console console, final MessageResource messageResource) {
        checkNotNull(console, "console must not be null");
        checkNotNull(messageResource, "messageResource must not be null");

        this.console = console;
        this.messageResource = messageResource;
    }

    @Override
    public Gesture throwGesture() {
        final List<String> symbols =
                new ArrayList<String>(Gesture.values().length);
        for (final Gesture gesture : Gesture.values()) {
            symbols.add(gesture.getSymbol());
        }
        final String userChoice = ConsoleUtils
                .askUserAndGetChoice(console, messageResource, "throw.gesture",
                        symbols.toArray(new String[symbols.size()]));
        return Gesture.fromSymbol(userChoice);
    }

}
