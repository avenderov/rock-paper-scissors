package com.gmail.avenderov;

import com.gmail.avenderov.factories.GameFactory;
import com.gmail.avenderov.utils.Console;
import com.gmail.avenderov.utils.ConsoleUtils;
import com.gmail.avenderov.utils.MessageResource;

import static com.gmail.avenderov.utils.Preconditions.checkNotNull;

/**
 * Main application class.
 *
 * @author avenderov
 */
public class RockPaperScissors {

    private static final String YES = "Y";

    private static final String NO = "N";

    private final Console console;

    private final MessageResource messageResource;

    private final GameTypeSelector gameTypeSelector;

    private final GameFactory gameFactory;

    public RockPaperScissors(final Console console,
                             final MessageResource messageResource,
                             final GameTypeSelector gameTypeSelector,
                             final GameFactory gameFactory) {
        checkNotNull(console, "console must not be null");
        checkNotNull(messageResource, "messageResource must not be null");
        checkNotNull(gameTypeSelector, "gameTypeSelector must not be null");
        checkNotNull(gameFactory, "gameFactory must not be null");

        this.console = console;
        this.messageResource = messageResource;
        this.gameTypeSelector = gameTypeSelector;
        this.gameFactory = gameFactory;
    }

    public void play() {
        displayBanner();

        boolean playAgain;
        do {
            final GameType gameType = gameTypeSelector.nextGameType();
            console.printf(messageResource.getString("game.type"),
                    messageResource.getString(
                            gameType.getDescriptionKey()));
            final Game game = gameFactory.createGame(gameType);
            game.start();

            playAgain = getPlayAgain();
        } while (playAgain);
    }

    private void displayBanner() {
        console.printf(messageResource.getString("application.banner"));
        console.readLine(messageResource.getString("press.enter"));
    }

    private boolean getPlayAgain() {
        final String userChoice = ConsoleUtils
                .askUserAndGetChoice(console, messageResource, "play.again",
                        new String[]{YES, NO});
        return YES.equalsIgnoreCase(userChoice);
    }

}
