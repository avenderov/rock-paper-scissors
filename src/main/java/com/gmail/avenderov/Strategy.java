package com.gmail.avenderov;

/**
 * Strategy that is used by a computer player to make next move.
 *
 * @author avenderov
 */
public interface Strategy {

    /**
     * Generates next gesture.
     *
     * @return {@link Gesture} instance. Never {@code null}
     */
    Gesture nextGesture();

}
