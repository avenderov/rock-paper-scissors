package com.gmail.avenderov;

import com.gmail.avenderov.utils.Console;
import com.gmail.avenderov.utils.MessageResource;
import com.gmail.avenderov.utils.Pair;

import static com.gmail.avenderov.utils.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 * This class represents one game round.
 *
 * @author avenderov
 */
public class Game {

    private final Console console;

    private final MessageResource messageResource;

    private final Pair<Player, Player> players;

    private final Arbiter arbiter;

    public Game(final Console console, final MessageResource messageResource,
                final Pair<Player, Player> players, final Arbiter arbiter) {
        checkNotNull(console, "console must not be null");
        checkNotNull(messageResource, "messageResource must not be null");
        checkNotNull(players, "players must not be null");
        checkNotNull(arbiter, "arbiter must not be null");

        this.console = console;
        this.messageResource = messageResource;
        this.players = players;
        this.arbiter = arbiter;
    }

    /**
     * Starts the game and prints results.
     */
    public void start() {
        console.printf(messageResource.getString("start.game"));

        final Gesture firstPlayerGesture = players.getFirst().throwGesture();
        final Gesture secondPlayerGesture = players.getSecond().throwGesture();

        console.printf(messageResource.getString("first.player.gesture"),
                firstPlayerGesture.toString());
        console.printf(messageResource.getString("second.player.gesture"),
                secondPlayerGesture.toString());

        final Arbiter.GameResult gameResult = arbiter.judge(firstPlayerGesture,
                secondPlayerGesture);

        switch (gameResult) {
            case DRAW:
                console.printf(messageResource.getString("result.draw"));
                break;
            case WON:
                console.printf(messageResource.getString("result.won"),
                        firstPlayerGesture.toString(),
                        secondPlayerGesture.toString());
                break;
            case LOST:
                console.printf(messageResource.getString("result.lost"),
                        secondPlayerGesture.toString(),
                        firstPlayerGesture.toString());
                break;
            default:
                throw new UnsupportedOperationException(
                        format("%1$s - game result is not supported",
                                gameResult));
        }

        console.printf(messageResource.getString("game.completed"));
    }

}
