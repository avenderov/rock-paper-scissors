package com.gmail.avenderov.factories;

import com.gmail.avenderov.Computer;
import com.gmail.avenderov.Player;
import com.gmail.avenderov.Strategy;
import com.gmail.avenderov.User;
import com.gmail.avenderov.utils.Console;
import com.gmail.avenderov.utils.MessageResource;

import static com.gmail.avenderov.utils.Preconditions.checkNotNull;

/**
 * This factory is used to create different {@link Player} implementations.
 *
 * @author avenderov
 */
public class PlayerFactory {

    private final Console console;

    private final MessageResource messageResource;

    private final Strategy strategy;

    public PlayerFactory(final Console console,
                         final MessageResource messageResource,
                         final Strategy strategy) {
        checkNotNull(console, "console must not be null");
        checkNotNull(messageResource, "messageResource must not be null");
        checkNotNull(strategy, "strategy must not be null");

        this.console = console;
        this.messageResource = messageResource;
        this.strategy = strategy;
    }

    /**
     * Returns {@link Player} implementation that represents human player.
     *
     * @return an instance of {@link User} class
     */
    public Player createUserPlayer() {
        return new User(console, messageResource);
    }

    /**
     * Returns {@link Player} implementation that represents computer player.
     *
     * @return an instance of {@link Computer} class
     */
    public Player createComputerPlayer() {
        return new Computer(strategy);
    }

}
