package com.gmail.avenderov.factories;

import com.gmail.avenderov.Arbiter;
import com.gmail.avenderov.Game;
import com.gmail.avenderov.GameType;
import com.gmail.avenderov.utils.Console;
import com.gmail.avenderov.utils.MessageResource;
import com.gmail.avenderov.utils.Pair;

import static com.gmail.avenderov.utils.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 * This factory is used to create {@link Game} instances.
 *
 * @author avenderov
 */
public class GameFactory {

    private final Console console;

    private final MessageResource messageResource;

    private final PlayerFactory playerFactory;

    private final Arbiter arbiter;

    public GameFactory(final Console console,
                       final MessageResource messageResource,
                       final PlayerFactory playerFactory,
                       final Arbiter arbiter) {
        checkNotNull(console, "console must not be null");
        checkNotNull(messageResource, "messageResource must not be null");
        checkNotNull(playerFactory, "playerFactory must not be null");
        checkNotNull(arbiter, "arbiter must not be null");

        this.console = console;
        this.messageResource = messageResource;
        this.playerFactory = playerFactory;
        this.arbiter = arbiter;
    }

    /**
     * Creates new {@link Game} instances based on specified game type. Each
     * invocation creates a new object.
     *
     * @param gameType a type of game to create
     *
     * @return properly configured instance of {@link Game} class
     * @throws NullPointerException if game type is {@code null}
     */
    public Game createGame(final GameType gameType) {
        checkNotNull(gameType, "game type must not be null");

        switch (gameType) {
            case COMPUTER_VS_COMPUTER:
                return new Game(console, messageResource,
                        Pair.of(playerFactory.createComputerPlayer(),
                                playerFactory.createComputerPlayer()), arbiter);
            case COMPUTER_VS_USER:
                return new Game(console, messageResource,
                        Pair.of(playerFactory.createUserPlayer(),
                                playerFactory.createComputerPlayer()), arbiter);
            default:
                throw new UnsupportedOperationException(
                        format("%1$s - game type is not supported", gameType));
        }
    }

}
