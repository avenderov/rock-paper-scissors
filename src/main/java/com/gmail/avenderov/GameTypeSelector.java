package com.gmail.avenderov;

/**
 * This class is used to generate next game type.
 *
 * @author avenderov
 * @see GameType
 */
public interface GameTypeSelector {

    /**
     * Generates next game type.
     *
     * @return a type of the next game
     */
    GameType nextGameType();

}
