package com.gmail.avenderov.exceptions;

/**
 * Checked exception that should be thrown if specified resource bundle was not
 * found.
 *
 * @author avenderov
 */
public class ResourceBundleNotFoundException extends Exception {

    public ResourceBundleNotFoundException(final String message,
                                           final Throwable cause) {
        super(message, cause);
    }

}
