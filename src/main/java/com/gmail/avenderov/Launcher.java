package com.gmail.avenderov;

import com.gmail.avenderov.factories.GameFactory;
import com.gmail.avenderov.factories.PlayerFactory;
import com.gmail.avenderov.utils.ClassPathMessageResource;
import com.gmail.avenderov.utils.DelegatingConsole;
import com.gmail.avenderov.utils.MessageResource;

import java.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to launch application.
 *
 * @author avenderov
 */
public class Launcher {

    private static final Logger LOGGER =
            Logger.getLogger(Launcher.class.getName());

    private static final String MESSAGES_BUNDLE = "messages";

    public static void main(String[] args) {
        LOGGER.log(Level.INFO, "Trying to obtain console...");
        final Console console = System.console();
        if (console != null) {
            try {
                LOGGER.log(Level.INFO,
                        "Console obtained, starting the game...");

                // Creating and wiring all dependencies. The only places where
                // 'new' is used are here and in *Factory classes
                final DelegatingConsole delegatingConsole =
                        new DelegatingConsole(console);
                final MessageResource messageResource =
                        ClassPathMessageResource.of(MESSAGES_BUNDLE);
                final GameTypeSelector gameTypeSelector =
                        new RandomGameTypeSelector();
                final PlayerFactory playerFactory =
                        new PlayerFactory(delegatingConsole, messageResource,
                                new RandomGestureStrategy());
                final GameFactory gameFactory =
                        new GameFactory(delegatingConsole, messageResource,
                                playerFactory, new DefaultArbiter());

                new RockPaperScissors(delegatingConsole, messageResource,
                        gameTypeSelector, gameFactory).play();
                LOGGER.log(Level.INFO, "Exiting the game");
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Unexpected exception occurred", e);
                // By default logger is configured to write into a file, so
                // user wouldn't see an exception on the screen. We have to
                // print it to the screen manually.
                e.printStackTrace();
                System.exit(1);
            }
        } else {
            LOGGER.log(Level.SEVERE,
                    "Failed to obtain console, can't start application");
            System.exit(1);
        }
    }

}
