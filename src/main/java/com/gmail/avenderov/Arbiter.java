package com.gmail.avenderov;

/**
 * Implementations of this interface are used to determine game result.
 *
 * @author avenderov
 */
public interface Arbiter {

    /**
     * Compares users gestures and returns a game result relative to the first
     * user. If first gesture beats second then {@link GameResult#WON} will be
     * returned, of second gesture beats first then {@link GameResult#LOST} will
     * be returned.
     *
     * @param firstGesture  gesture thrown by the first player
     * @param secondGesture gesture thrown by the second player
     *
     * @return {@link GameResult#WON} if {@code firstGesture} beats {@code
     *         secondGesture}, {@link GameResult#LOST} if {@code secondGesture}
     *         beats {@code firstGesture}, {@link GameResult#DRAW} if gestures
     *         are equal
     */
    GameResult judge(Gesture firstGesture, Gesture secondGesture);

    public enum GameResult {

        WON, LOST, DRAW

    }

}
