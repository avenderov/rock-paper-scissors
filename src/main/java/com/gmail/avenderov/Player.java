package com.gmail.avenderov;

/**
 * Interface that defines a player that takes part in a game.
 *
 * @author avenderov
 */
public interface Player {

    /**
     * "Throws" a new gesture for the current move.
     *
     * @return {@link Gesture} instance. Never {@code null}
     */
    Gesture throwGesture();

}
