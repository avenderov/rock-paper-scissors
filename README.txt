Rock Paper Scissors game.

Author: Alexey Venderov

1. User Story Front
2. User Story Back
3. Prerequisites
4. How to build?
5. How to run?
6. Notes

1. User Story Front

    +--------------------------------------------------+
    |                                                  |
    |     Title: Waste an Hour Having Fun              |
    |                                                  |
    | As a frequent games player,                      |
    | I'd like to play rock, paper, scissors           |
    | so that I can spend an hour of my day having fun |
    |                                                  |
    | Acceptance Criteria                              |
    |  - Can I play Player vs Computer?                |
    |  - Can I play Computer vs Computer?              |
    |  - Can I play a different game each time?        |
    |                                                  |
    |                                                  |
    +--------------------------------------------------+

2. User Story Back

    +--------------------------------------------------+
    |                                                  |
    |                                                  |
    |                                                  |
    | Technical Constraints                            |
    |                                                  |
    | - Doesn't necessarily need a flashy GUI          |
    |   (can be simple)                                |
    | - Use Java                                       |
    | - Libs / external modules should only be used    |
    |   for tests                                      |
    | - Using best in industry agile engineering       |
    |   practices                                      |
    |                                                  |
    |                                                  |
    |                                                  |
    +--------------------------------------------------+

3. Prerequisites

    To build and run the project java 1.6 and maven has to be installed.

4. How to build?

    Maven is used to build the project. In project root directory run:

        mvn package

5. How to run?

    Project has exec-maven-plugin configured in pom.xml. To start the game, in
    project root directory, run:

        mvn exec:java

    Also the project can be run manually. After building the project, go to the
    folder target/classes, under the project root directory and run:

         java com.gmail.avenderov.Launcher

    or

        java -Djava.util.logging.config.file=logging.properties com.gmail.avenderov.Launcher

    to use custom logging.properties. Java has to be in PATH in that case.

6. Notes

    6.1 For logging I used standard java.util.logging. In real life project,
        where it is possible to use external dependencies, I prefer to use slf4j
        as logging facade. In this case implementation can be easily swapped if
        needed.
    6.2 I'm not creating complex dependencies directly. I use object factories
        instead. It complicates the code a little bit, but code remains testable.
        Factories can easily be mocked in tests.
